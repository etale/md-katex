$`\hbar/i = 1`$

$`G`$を重力定数、$`c`$を真空中の光速、$`\varepsilon_0`$を電気定数、$`\mu_0`$を磁気定数、$`k`$をボルツマン定数、$`h`$をプランク定数とする。
これらの普遍定数は以下の値であれば諸々の式がシンプルになる。

```math
4\pi G = c = \varepsilon_0 = \mu_0 = k = h/2\pi i = 1
```
2019-05-20以降、$`c, k, h`$は定義値となった。また、電気素量$`e`$も定義値となったので、国際単位系における質量・温度・長さ・時間は正確にeVに換算できる。
```math
\begin{aligned}
  G&=\nu_G\ {\rm kg}^{-1}\cdot{\rm m}^3\cdot{\rm s}^{-2} \\
  \mu_0 &= \nu_{\mu_0}\ {\rm kg}\cdot{\rm m}\cdot{\rm C}^{-2} \\
  \\
  {\rm eV}&=\nu_e\ {\rm kg}\cdot{\rm m}^2\cdot{\rm s}^{-2} \\
  c&=\nu_c\ {\rm m}\cdot{\rm s}^{-1} \\
  k&=\nu_k\ {\rm kg}\cdot{\rm m}^2\cdot{\rm s}^{-2}\cdot{\rm K}^{-1} \\
  h&=\nu_h\ {\rm kg}\cdot{\rm m}^2\cdot{\rm s}^{-1} \\
  \\
  {\rm kg} &= \nu_c^2 \ {\rm J} \\
  {\rm K} &= \nu_k \ {\rm J} \\
  {\rm m} &= 2\pi i \nu_c^{-1} \nu_h^{-1} \ {\rm J}^{-1} \\
  {\rm s} &= 2\pi i \nu_h^{-1} \ {\rm J}^{-1} \\
  {\rm C}^2 &= 2\pi i \nu_{\mu_0} \nu_c \nu_h^{-1} \\
\end{aligned}
```
